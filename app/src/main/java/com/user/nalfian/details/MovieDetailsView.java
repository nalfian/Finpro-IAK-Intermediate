package com.user.nalfian.details;

import com.user.nalfian.Movie;
import com.user.nalfian.Review;
import com.user.nalfian.Video;

import java.util.List;

/**
 * @author arun
 */
interface MovieDetailsView
{
    void showDetails(Movie movie);
    void showTrailers(List<Video> trailers);
    void showReviews(List<Review> reviews);
    void showFavorited();
    void showUnFavorited();
}
