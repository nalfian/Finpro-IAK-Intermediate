package com.user.nalfian;

import com.user.nalfian.details.DetailsComponent;
import com.user.nalfian.details.DetailsModule;
import com.user.nalfian.favorites.FavoritesModule;
import com.user.nalfian.listing.ListingComponent;
import com.user.nalfian.listing.ListingModule;
import com.user.nalfian.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author arunsasidharan
 * @author pulkitkumar
 */
@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        FavoritesModule.class})
public interface AppComponent
{
    DetailsComponent plus(DetailsModule detailsModule);

    ListingComponent plus(ListingModule listingModule);
}
