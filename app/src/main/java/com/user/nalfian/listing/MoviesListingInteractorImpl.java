package com.user.nalfian.listing;


import com.user.nalfian.Movie;
import com.user.nalfian.MoviesWraper;
import com.user.nalfian.favorites.FavoritesInteractor;
import com.user.nalfian.listing.sorting.SortType;
import com.user.nalfian.listing.sorting.SortingOptionStore;
import com.user.nalfian.network.TmdbWebService;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author arun
 */
class MoviesListingInteractorImpl implements MoviesListingInteractor {
    private FavoritesInteractor favoritesInteractor;
    private TmdbWebService tmdbWebService;
    private SortingOptionStore sortingOptionStore;

    MoviesListingInteractorImpl(FavoritesInteractor favoritesInteractor,
                                TmdbWebService tmdbWebService, SortingOptionStore store) {
        this.favoritesInteractor = favoritesInteractor;
        this.tmdbWebService = tmdbWebService;
        sortingOptionStore = store;
    }

    @Override
    public Observable<List<Movie>> fetchMovies() {
        int selectedOption = sortingOptionStore.getSelectedOption();
        if (selectedOption == SortType.MOST_POPULAR.getValue()) {
            return tmdbWebService.popularMovies().map(MoviesWraper::getMovieList);
        } else if (selectedOption == SortType.HIGHEST_RATED.getValue()) {
            return tmdbWebService.highestRatedMovies().map(MoviesWraper::getMovieList);
        } else if (selectedOption == SortType.ACTION.getValue()) {
            return tmdbWebService.actionMovies().map(MoviesWraper::getMovieList);
        } else if (selectedOption == SortType.ADVENTURE.getValue()) {
            return tmdbWebService.adventureMovies().map(MoviesWraper::getMovieList);
        }else if (selectedOption == SortType.ANIMATION.getValue()) {
            return tmdbWebService.animationMovies().map(MoviesWraper::getMovieList);
        } else {
            return Observable.just(favoritesInteractor.getFavorites());
        }
    }

}
