package com.user.nalfian.listing.sorting;

/**
 * @author arun
 */
interface SortingDialogView
{
    void setPopularChecked();

    void setHighestRatedChecked();

    void setFavoritesChecked();

    void dismissDialog();

}
