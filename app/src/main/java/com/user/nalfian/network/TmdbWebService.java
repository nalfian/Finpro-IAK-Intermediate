package com.user.nalfian.network;


import com.user.nalfian.MoviesWraper;
import com.user.nalfian.ReviewsWrapper;
import com.user.nalfian.VideoWrapper;

import retrofit2.http.GET;
import retrofit2.http.Path;
import io.reactivex.Observable;

/**
 * Created by ivan on 8/20/2017.
 */

public interface TmdbWebService {

    @GET("3/discover/movie?language=en&sort_by=popularity.desc")
    Observable<MoviesWraper> popularMovies();

    @GET("3/discover/movie?vote_count.gte=500&language=en&sort_by=vote_average.desc")
    Observable<MoviesWraper> highestRatedMovies();

    @GET("3/genre/28/movies")
    Observable<MoviesWraper> actionMovies();

    @GET("3/genre/12/movies")
    Observable<MoviesWraper> adventureMovies();

    @GET("3/genre/16/movies")
    Observable<MoviesWraper> animationMovies();


    @GET("3/movie/{movieId}/videos")
    Observable<VideoWrapper> trailers(@Path("movieId") String movieId);

    @GET("3/movie/{movieId}/reviews")
    Observable<ReviewsWrapper> reviews(@Path("movieId") String movieId);

}
